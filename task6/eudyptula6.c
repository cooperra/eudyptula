#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/miscdevice.h>
#include <linux/fs.h>

MODULE_AUTHOR("Riley Cooper");
MODULE_DESCRIPTION("eudyptula task6\n");
MODULE_LICENSE("GPL");

static char eudyptula_string[] = "eudyptula\n";

ssize_t read_eudyptula (struct file *, char __user *, size_t, loff_t *);
ssize_t write_eudyptula (struct file *, const char __user *, size_t, loff_t *);
int open_eudyptula (struct inode *, struct file *);
int release_eudyptula (struct inode *, struct file *);

struct file_operations my_misc_fops = {
    .owner = THIS_MODULE,
    .read = read_eudyptula,
    .write = write_eudyptula,
    .open = open_eudyptula,
    .release = release_eudyptula,
    .llseek = no_llseek,
};

struct miscdevice my_misc_device = {
    .minor = MISC_DYNAMIC_MINOR,
    .fops = &my_misc_fops,
    .name = "eudyptula",
};

static int __init misc_char_init (void)
{
    int register_result;

    register_result = misc_register(&my_misc_device);
    if (register_result) {
        printk("register misc eudyptula driver failed error: %d\n", register_result);
        return -ENODEV;
    }
    printk("eudyptula misc driver registered successfully\n");
    return 0;
}

static void __exit misc_char_exit (void)
{
    misc_deregister(&my_misc_device);
    printk("eudyptula misc device deregistered\n");
}

ssize_t read_eudyptula (struct file * filp, char __user * buf, size_t count, loff_t * f_pos)
{
    return simple_read_from_buffer(buf, strlen(eudyptula_string), f_pos, eudyptula_string, strlen(eudyptula_string));
}

ssize_t write_eudyptula (struct file * filp, const char __user * buf, size_t count, loff_t * f_pos)
{
    int i;
    for (i = 0; i < strlen(eudyptula_string) && i < count; i++) {
        if (buf[i] != eudyptula_string[i]) {
            return -EINVAL;
        }
    }
    if (i != strlen(eudyptula_string)) {
        return -EINVAL;
    }
    return count;
}

int open_eudyptula (struct inode * inode_p, struct file * filp)
{
    return 0;
}

int release_eudyptula (struct inode * inode_p, struct file * filp)
{
    return 0;
}

module_init(misc_char_init);
module_exit(misc_char_exit);
