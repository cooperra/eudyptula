#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/usb.h>

// New USB device found, idVendor=413c, idProduct=2003, bcdDevice= 3.01
//#define V_ID 0x413c
//#define P_ID 0x2003
//8086:2835
#define V_ID 0x8086
#define P_ID 0x2835

MODULE_AUTHOR("Riley Cooper");
MODULE_DESCRIPTION("eudyptula task5\n");
MODULE_LICENSE("GPL");

int keyboard_connect (struct usb_interface *intf, const struct usb_device_id *id);
void keyboard_disconnect (struct usb_interface *intf);

struct usb_device_id keyboard_id_table[] = {{USB_DEVICE(V_ID, P_ID)}, {}};
MODULE_DEVICE_TABLE(usb, keyboard_id_table);

struct usb_driver task5_keyboard_driver = {
    .name = "eudyptula_keyboard",
    .probe = keyboard_connect,
    .disconnect = keyboard_disconnect,
    .id_table = keyboard_id_table
};

static int __init init_usb_module (void)
{
    int err;

    printk("Keyboard module init\n");
    err = usb_register(&task5_keyboard_driver);
    if (err) {
        printk("Error with registering USB keyboard driver\n");
    }
    return 0;
}

static void __exit exit_usb_module (void)
{
    printk("Keyboard module exit\n");
    usb_deregister (&task5_keyboard_driver);
}

int keyboard_connect (struct usb_interface *intf, const struct usb_device_id *id)
{
    printk("Keyboard connected\n");
    return 0;
}

void keyboard_disconnect (struct usb_interface *intf)
{
    printk("Keyboard disconnected\n");
}

module_init(init_usb_module);
module_exit(exit_usb_module);
